'use strict';

var Redis = require("ioredis");
var redis = new Redis(6379, '127.0.0.1');

redis.del('room_count');
redis.del('room_force');
redis.del('room_players');
redis.del('rooms');
redis.del('player_room');
redis.del('room_update');
redis.del('room_king');
redis.del('king');