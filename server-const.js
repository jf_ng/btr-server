'use strict';

var Const = {
	room_max: 20,
	
    king_interval: 180000, // ms @todo
    king_duration: 60000 // ms @todo
}

module.exports = Const;