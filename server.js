'use strict';

var Const = require('./server-const.js');

// var os = require('os');
// var mem = require('free-memory');

var cors = require('cors');
var http = require('http');
var mysql = require('mysql');
var express = require('express');
var df = require('dateformat');
var jwt = require('jsonwebtoken');

var hmacSHA512 = require("crypto-js/hmac-sha512");
var Base64 = require('crypto-js/enc-base64');

var multer  = require('multer');
var upload = multer({ dest: 'uploads/' });

var moment = require('moment');

var app_secret = 'T+-fEMoFrO?p*y1?7B2E';
var jwt_secret = 'Cr02R$c#49Ebi?E+A9_J';

var app = express();
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

// var corsOptions = {
//   origin: '*',
//   optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204 
// }
// app.use(cors(corsOptions));

/**
* do not use this in production
* should use SSL LB
*/
// var fs = require('fs');
// var options = {
//     key: fs.readFileSync( './localhost_key.pem' ),
//     cert: fs.readFileSync( './localhost_cert.pem' ),
//     requestCert: false,
//     rejectUnauthorized: false
// };
// var https = require('https');

// var server = https.createServer(options, app);
var server = http.createServer(app);
var io = require('socket.io')(server, {
    path: '/btr',
    pingTimeout: 2000,
    pingInterval: 5000
});

var redisAdapter = require('socket.io-redis');
io.adapter(redisAdapter({
    key: 'btr',
    host: '127.0.0.1',
    port: 6379
}));
const btr = io.of('/btr');

var Redis = require("ioredis");
var redis = new Redis(6379, '127.0.0.1');

var rds = mysql.createPool({
    connectionLimit : 10,
    host     : 'localhost',
    user     : 'root',
    password : 'password',
    database : 'btr'
});


/**
**************************************************
**************************************************
*/

var p1 = new Promise(function(resolve, reject) {
    rds.query('SELECT id, name, price, description, content, DATE_FORMAT(datetime_start, \'%Y-%m-%d %T\') AS datetime_start, DATE_FORMAT(datetime_end, \'%Y-%m-%d %T\') AS datetime_end FROM iap', [], function (error, results, fields) {
        if (error) {
            reject(error);
        }
        else {
            resolve(results);
        }
    });
});

// rds.query('SELECT * FROM mst_item', [], function (error, results, fields) {
//     for (var i = 0; i < results.length; i++) {
//         redis.hset('mst_item', results[i].id, JSON.stringify(results[i]));
//     }
// });


// var name_english = [];
// rds.query('SELECT * FROM idle_name WHERE category="English"', [], function (error, results, fields) {
//     for (var i = 0; i < results.length; i++) {
//         name_english.push(results[i].name);
//     }
// });

// var name_chinese = [];
// rds.query('SELECT * FROM idle_name WHERE category="Chinese"', [], function (error, results, fields) {
//     for (var i = 0; i < results.length; i++) {
//         name_chinese.push(results[i].name);
//     }
// });

// rds.query('SELECT * FROM idle_item', [], function (error, results, fields) {
//     var items = results;
//     for (var i = 0; i < items.length; i++) {
//         redis.hset('item', items[i].id, JSON.stringify(items[i]));
//     }

//     var p1 = new Promise(function(resolve, reject) {
//         rds.query('SELECT id, name, details, price, currency, category, DATE_FORMAT(datetime_start, \'%Y-%m-%d %T\') AS datetime_start, DATE_FORMAT(datetime_end, \'%Y-%m-%d %T\') AS datetime_end FROM idle_iap', [], function (error, results, fields) {
//             if (error) {
//                 reject(error);
//             }
//             else {
//                 resolve(results);
//             }
//         });
//     });

//     var p2 = new Promise(function(resolve, reject) {
//         rds.query('SELECT * FROM idle_iap_item', [], function (error, results, fields) {
//             if (error) {
//                 reject(error);
//             }
//             else {
//                 resolve(results);
//             }
//         });
//     });

let promise = Promise.all([p1]);
promise.then(function(data) {
    var iap = data[0];
    var now = new Date();

    for (var i = 0; i < iap.length; i++) {
        let dateTimeParts = String(iap[i].datetime_start).split(/[- :]/); // regular expression split that creates array with: year, month, day, hour, minutes, seconds values
        dateTimeParts[1]--; // monthIndex begins with 0 for January and ends with 11 for December so we need to decrement by one

        let start = new Date(...dateTimeParts); // our Date object

        dateTimeParts = String(iap[i].datetime_end).split(/[- :]/); // regular expression split that creates array with: year, month, day, hour, minutes, seconds values
        dateTimeParts[1]--; // monthIndex begins with 0 for January and ends with 11 for December so we need to decrement by one

        let end = new Date(...dateTimeParts); // our Date object

        if (start.getTime() <= now.getTime() && end.getTime() > now.getTime()) {
            var json = {id: iap[i].id, name: iap[i].name, price: iap[i].price, description: iap[i].description, content: iap[i].content, start: iap[i].datetime_start, end: iap[i].datetime_end, items: []}

            redis.hset('iap', iap[i].id, JSON.stringify(json));
        }
    }
}).catch(function(error) {
    console.log(error);
    process.exit(1);
});


// Load and instantiate Chance
var chance = require('chance').Chance();

/**
**************************************************
**************************************************
*/


// server.listen(80, 'localhost');
server.listen(80, '192.168.1.119');

app.get('/test', function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify({status: 'ok'}));
});

app.get('/hc', function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify({status: 'success'}));
});

app.post('/hc', function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify({status: 'success'}));
});


function log(level, message) {
    console.log(message);
}



/**
**************************************************
**************************************************
*/



app.post('/register', function(req, res) {
    rds.getConnection(function(err, connection) {
        connection.beginTransaction(function(err) {

            // **************************************************
            // generate unique game_id
            // **************************************************
            new Promise(function(resolve, reject) {
                var loop = true;
                for (var i = 0; loop && i < 10; i++) {
                    var game_id = chance.integer({ min: 10000000, max: 99999999 });
                    redis.sismember('game_id', game_id).then(function(exist) {
                        if (!exist) {
                            resolve(game_id);
                            loop = false;
                        }
                        else {
                            if (i == 9) {
                                reject('unable to get unique game_id');
                            }
                        }
                    });
                }
            }).then(function(game_id) {
                var salt = chance.string({ length: 5 });

                // create token
                var token = jwt.sign({ user_id: req.body.user_id, salt: salt }, jwt_secret, { algorithm: 'HS256'});


                // **************************************************
                // create user
                // **************************************************
                var post = req.body;
                post.game_id = game_id;
                post.salt = salt;
                post.token = token;

                if (!post.name || post.name == 'null') {
                    post.name = 'p_' + game_id;
                }
                if (!post.email || post.email == 'null') {
                    delete post.email;
                }
                if (!post.phone || post.phone == 'null') {
                    delete post.phone;
                }
                if (!post.photo || post.photo == 'null') {
                    delete post.photo;
                }
                else {
                    post.photo = decodeURIComponent(post.photo);
                }


                // speed
                var speed = post.speed; delete post.speed;
                var laser = post.laser; delete post.laser;
                var tank = post.tank; delete post.tank;
                var king = post.king; delete post.king;


                var p1 = new Promise(function(resolve, reject) {
                    connection.query('INSERT INTO user SET ?', post, function (error, results, fields) {
                        if (error) {
                            reject(error);
                        }
                        else {
                            resolve();
                        }
                    });
                });

                var p2 = new Promise(function(resolve, reject) {
                    redis.hset('user_token', token, JSON.stringify({user_id: req.body.user_id, game_id: game_id}));
                    resolve();
                });

                var p3 = new Promise(function(resolve, reject) {
                    redis.sadd('game_id', game_id);
                    resolve();
                });


                // **************************************************
                // create score
                // **************************************************
                var p4 = new Promise(function(resolve, reject) {
                    var post = {
                        user_id: req.body.user_id
                    }

                    var points = 0;

                    if (tank) {
                        post.tank = parseInt(tank);
                        points += parseInt(tank);
                    }

                    if (king) {
                        post.eagle = parseInt(king);
                        points += parseInt(king);
                    }

                    post.points = points;

                    connection.query('INSERT INTO score SET ?', post, function (error, results, fields) {
                        if (error) {
                            reject(error);
                        }
                        else {
                            resolve();
                        }
                    });
                });


                // **************************************************
                // create inventory
                // **************************************************
                var p5 = new Promise(function(resolve, reject) {
                    var post = {
                        user_id: req.body.user_id,
                        laser: (speed) ? speed : 10,
                        speed: (laser) ? laser : 5
                    }

                    connection.query('INSERT INTO inventory SET ?', post, function (error, results, fields) {
                        if (error) {
                            reject(error);
                        }
                        else {
                            resolve();
                        }
                    });
                });

                // **************************************************
                // create user data
                // **************************************************
                var p6 = new Promise(function(resolve, reject) {
                    var post = {
                        user_id: req.body.user_id,
                        last_login: moment().format("YYYY-MM-DD HH:mm:ss")
                    }

                    connection.query('INSERT INTO user_data SET ?', post, function (error, results, fields) {
                        if (error) {
                            reject(error);
                        }
                        else {
                            resolve();
                        }
                    });
                });


                let promise = Promise.all([p1, p2, p3, p4, p5, p6]);
                promise.then(function(data) {
                    connection.commit(function(error) {
                        if (error) {
                            throw error;
                        }
                        else {
                            connection.release();

                            res.send(JSON.stringify({status: 'success', token: token, game_id: game_id, user_id: req.body.user_id}));
                        }
                    });
                }).catch(function(error) {
                    connection.rollback(function() {});
                    connection.release();

                    log('critical', error);
                    res.send(JSON.stringify({status: 'error'}));
                });
            }).catch(function(error) {
                connection.rollback(function() {});
                connection.release();

                log('critical', error);
                res.send(JSON.stringify({status: 'error'}));
            });
        });
    });
});


function checkUserToken(token, game_id) {
    return new Promise(function(resolve, reject) {
        redis.hget('user_token', token).then(function(user) {
            if (!user) {
                reject('invalid token');
            }
            else {
                user = JSON.parse(user);
                if (user.game_id != game_id) {
                    reject('invalid game id [' + user.game_id + '   ' + game_id + ']');
                }
                else {
                    resolve(user.user_id);
                }
            }
        });
    });
}


function changeTimezone(date) {
    // suppose the date is 12:00 UTC
    var invdate = new Date(date.toLocaleString('en-US', {
        timeZone: 'Asia/Singapore'
    }));

    // then invdate will be 07:00 in Toronto
    // and the diff is 5 hours
    var diff = date.getTime() - invdate.getTime();

    // so 12:00 in Toronto is 17:00 UTC
    return new Date(date.getTime() + diff);
}


function recoverGeneralEnergy(general, mst) {
    if (general.energy_last_recovered != null) {
        var now = new Date().toUTCString();

        var then = new Date(general.energy_last_recovered);
        then = new Date(then.getTime() - then.getTimezoneOffset() * 60 * 1000).toUTCString();

        var diff = (new Date(now).getTime() - new Date(then).getTime()) / 1000; // convert to seconds

        var recover = Math.floor(diff / mst.recover);
        if (recover > 0) {
            general.energy += recover;
            if (general.energy >= 20) {
                general.energy = 20;
                general.energy_last_recovered = null;
            }
            else {
                general.energy_last_recovered = new Date(new Date(then).getTime() + recover * mst.recover * 1000).toISOString().slice(0, 19).replace('T', ' ');
            }

        }
    }
    else {
        var utc = new Date().toUTCString();
        utc = new Date(new Date(utc).getTime()); // local time
        general.energy_last_recovered = utc.toISOString().slice(0, 19).replace('T', ' ');
    }

    return general;
}

function createAlert(connection, user_id, subject, content) {
    var p = new Promise(function(resolve, reject) {
        var param = {
            user_id: user_id,
            subject: subject,
            content: content,
            status: 'Unread'
        };

        connection.query('INSERT INTO alert SET ?', param, function (error, results, fields) {
            if (error) {
                reject(error);
            }
            else {
                resolve();
            }
        });
    });

    return p;
}



app.post('/initialize', function(req, res) {
    checkUserToken(req.body.token, req.body.game_id).then(function(user_id) {
        // **************************************************
        // get user data
        // **************************************************
        var game_id;

        new Promise(function(resolve, reject) {
            rds.query('SELECT * FROM user WHERE user_id = ?', [user_id], function (error, results, fields) {
                if (error || results.length == 0) {
                    log('critical', error);
                    reject('cannot find user');
                }
                else {
                    game_id = results[0].game_id;

                    var user = {
                        name: results[0].name,
                        photo: results[0].photo,
                        email: results[0].email,
                        language: results[0].language
                    }


                    // **************************************************
                    // get video ads
                    // **************************************************
                    var p1 = new Promise(function(resolve, reject) {
                        var start = new Date();
                        start.setHours(0,0,0,0);
                        start = new Date(new Date(start).getTime());
                        start = new Date(start.getTime() - start.getTimezoneOffset() * 60 * 1000).toISOString().slice(0, 19).replace('T', ' ');

                        var end = new Date();
                        end.setHours(23,59,59,999);
                        end = new Date(new Date(end).getTime());
                        end = new Date(end.getTime() - end.getTimezoneOffset() * 60 * 1000).toISOString().slice(0, 19).replace('T', ' ');

                        rds.query('SELECT count(*) as count FROM video_ads WHERE user_id = ? AND datetime_created >= ? AND datetime_created <= ?', [user_id, start, end], function (error, results, fields) {
                            if (error) {
                                log('critical', error);
                                reject('cannot find video ads');
                            }
                            else {
                                resolve(results[0].count);
                            }
                        });
                    });


                    // **************************************************
                    // get friend list
                    // **************************************************
                    var p2 = new Promise(function(resolve, reject) {
                        var pp = [], arr = [];

                        var friends = JSON.parse(req.body.friends);
                        for (var i = 0; i < friends.length; i++) {
                            arr.push(friends[i].id);
                        }

                        var pp1 = new Promise(function(resolve, reject) {
                            if (arr.length > 0) {
                                rds.query('SELECT user_id, name, photo FROM user WHERE fbinstant_id IN (?)', [arr], function (error, results, fields) {
                                    if (error) {
                                        log('critical', error);
                                        reject('cannot find friend list');
                                    }
                                    else {
                                        var users = results;
                                        var userids = [];
                                        for (var i = 0; i < users.length; i++) {
                                            userids.push(users[i].user_id);
                                        }


                                        if (userids.length > 0) {
                                            rds.query('SELECT friend_id FROM friend_list WHERE user_id = ? AND friend_id IN (?)', [user_id, userids], function (error, results, fields) {
                                                if (error) {
                                                    log('critical', error);
                                                    reject('cannot find friend list');
                                                }
                                                else {
                                                    for (var i = 0; i < users.length; i++) {
                                                        var friend = users[i];
                                                        var found = false;

                                                        for (var j = 0; j < results.length; j++) {
                                                            if (results[j].friend_id == friend.user_id) {
                                                                found = true;
                                                                break;
                                                            }
                                                        }

                                                        if (!found) {
                                                            // **************************************************
                                                            // create new friend
                                                            // **************************************************
                                                            var pp2 = new Promise(function(resolve, reject) {
                                                                var post = {};
                                                                post.user_id = user_id;
                                                                post.friend_id = friend.user_id;
                                                                post.friend_name = friend.name;
                                                                post.friend_photo = friend.photo;
                                                                post.status = 'accept';

                                                                rds.query('INSERT INTO friend_list SET ?', post, function (error, results, fields) {
                                                                    if (error) {
                                                                        reject(error);
                                                                    }
                                                                    else {
                                                                        resolve();
                                                                    }
                                                                });
                                                            });
                                                            pp.push(pp2);

                                                            var pp3 = new Promise(function(resolve, reject) {
                                                                var post = {};
                                                                post.user_id = friend.user_id;
                                                                post.friend_id = user_id;
                                                                post.friend_name = user.name;
                                                                post.friend_photo = user.photo;
                                                                post.status = 'accept';

                                                                rds.query('INSERT INTO friend_list SET ?', post, function (error, results, fields) {
                                                                    if (error) {
                                                                        reject(error);
                                                                    }
                                                                    else {
                                                                        resolve();
                                                                    }
                                                                });
                                                            });
                                                            pp.push(pp3);
                                                        }
                                                    }

                                                    resolve();
                                                }
                                            });
                                        }
                                        else resolve();
                                    }
                                });
                            }
                            else resolve();
                        });
                        pp.push(pp1);


                        let promise = Promise.all(pp);
                        promise.then(function(data) {
                            rds.query('SELECT * FROM friend_list WHERE user_id = ? AND (status=\'accept\' OR status = \'request\')', [user_id], function (error, results, fields) {
                                if (error) {
                                    log('critical', error);
                                    reject('cannot find friend list');
                                }
                                else {
                                    for (var i = 0; i < results.length; i++) {
                                        delete results[i].id;
                                        delete results[i].user_id;
                                        delete results[i].datetime_created;
                                        delete results[i].datetime_updated;
                                    }

                                    resolve(results);
                                }
                            });
                        }).catch(function(error) {
                            reject(error);
                        });
                    });


                    // **************************************************
                    // get friend ranking
                    // **************************************************
                    var p3 = new Promise(function(resolve, reject) {
                        rds.query('SELECT friend_id FROM friend_list WHERE user_id = ? AND status = \'accept\'', [user_id], function (error, results, fields) {
                            if (error) {
                                log('critical', error);
                                reject('cannot find score');
                            }
                            else {
                                var list = [user_id];
                                for (var i = 0; i < results.length; i++) {
                                    list.push(results[i].friend_id);
                                }

                                rds.query('SELECT * FROM score WHERE user_id IN (?) ORDER BY points DESC', [list], function (error, results, fields) {
                                    if (error) {
                                        log('critical', error);
                                        reject('cannot find score');
                                    }
                                    else {
                                        for (var i = 0; i < results.length; i++) {
                                            delete results[i].id;
                                        }

                                        resolve(results);
                                    }
                                });
                            }
                        });
                    });


                    // **************************************************
                    // get chat
                    // **************************************************
                    var p4 = new Promise(function(resolve, reject) {
                        rds.query('SELECT * FROM chat WHERE (user_id = ? AND user_status = \'active\') OR (friend_id = ? AND friend_status = \'active\')', [user_id, user_id], function (error, results, fields) {
                            if (error) {
                                log('critical', error);
                                reject('cannot find chat');
                            }
                            else {
                                if (results.length > 0) {
                                    var chats = results;

                                    var arr = [];
                                    for (var i = 0; i < results.length; i++) {
                                        arr.push(results[i].id);
                                    }

                                    rds.query('SELECT chat_id, sender, message, datetime_created FROM chat_message WHERE chat_id in (?) AND ( (sender = ? AND sender_status = \'active\') OR (sender != ? AND recipent_status = \'active\') ) ORDER BY id ASC', [arr, user_id, user_id], function (error, results, fields) {
                                        if (error) {
                                            log('critical', error);
                                            reject('cannot find chat messages');
                                        }
                                        else {
                                            resolve([chats, results]);
                                        }
                                    });
                                }
                                else {
                                    resolve([[], []]);
                                }
                            }
                        });
                    });


                    // **************************************************
                    // get iap
                    // **************************************************
                    var p5 = new Promise(function(resolve, reject) {
                        redis.hgetall('iap').then(function(iap) {
                            resolve(iap);
                        });
                    });


                    // **************************************************
                    // get inventory
                    // **************************************************
                    var p6 = new Promise(function(resolve, reject) {
                        rds.query('SELECT laser, speed FROM inventory WHERE user_id = ?', [user_id], function (error, results, fields) {
                            if (error) {
                                log('critical', error);
                                reject('cannot find inventory');
                            }
                            else {
                                if (req.body.update_inventory === 'true') {
                                    var inventory = results[0];
                                    var current_speed = 0, current_laser = 0;

                                    if (inventory.speed > req.body.speed) current_speed = req.body.speed;
                                    else current_speed = inventory.speed;

                                    if (inventory.laser > req.body.laser) current_laser = req.body.laser;
                                    else current_laser = inventory.laser;

                                    if (current_speed != inventory.speed || current_laser != inventory.laser) {
                                        rds.query('UPDATE inventory SET laser = ?, speed = ? WHERE user_id = ?', [current_laser, current_speed, user_id], function (error, results, fields) {
                                            if (error) {
                                                log('critical', error);
                                                reject('cannot update inventory');
                                            }
                                            else {
                                                resolve({
                                                    speed: current_speed,
                                                    laser: current_laser
                                                });
                                            }
                                        });
                                    }
                                    else {
                                        resolve(inventory);
                                    }
                                }
                                else {
                                    resolve(results[0]);
                                }
                            }
                        });
                    });

                    // **************************************************
                    // get user data
                    // **************************************************
                    var p7 = new Promise(function(resolve, reject) {
                        rds.query('SELECT * FROM user_data WHERE user_id = ?', [user_id], function (error, results, fields) {
                            if (error) {
                                log('critical', error);
                                reject('cannot find user data');
                            }
                            else {
                                delete results[0].id;

                                resolve(results[0]);
                            }
                        });
                    });

                    // **************************************************
                    // update user data
                    // **************************************************
                    var p8 = new Promise(function(resolve, reject) {
                        rds.query('UPDATE user_data SET last_login = ?, new_chat_message = 0, new_friend_request = 0 WHERE user_id = ?', [moment().format("YYYY-MM-DD HH:mm:ss"), user_id], function (error, results, fields) {
                            if (error) {
                                log('critical', error);
                                reject('cannot update user data');
                            }
                            else {
                                resolve();
                            }
                        });
                    });

                    
                    let promise = Promise.all([p1, p2, p3, p4, p5, p6, p7, p8]);
                    promise.then(function(data) {
                        res.send(JSON.stringify({
                            status: 'success',
                            user: user,
                            friends: data[1],
                            scores: data[2],
                            chats: data[3][0],
                            chat_messages: data[3][1],
                            iap: data[4],
                            inventory: data[5],
                            user_data: data[6]
                        }));
                    }).catch(function(error) {
                        log('critical', error);
                        res.send(JSON.stringify({status: 'error'}));
                    });
                }
            });
        }).catch(function(error) {
            log('critical', error);
            res.send(JSON.stringify({status: 'error'}));
        });
    }).catch(function(error) {
        log('critical', error);
        res.send(JSON.stringify({status: 'error'}));
    });
});

app.post('/changeName', function(req, res) {
    checkUserToken(req.body.token, req.body.game_id).then(function(user_id) {
        rds.getConnection(function(err, connection) {
            connection.beginTransaction(function(err) {
                var name = req.body.name.trim().substring(0, 10);

                var p1 = new Promise(function(resolve, reject) {
                    connection.query('UPDATE user SET name = ? WHERE user_id = ?', [name, user_id], function (error, results, fields) {
                        if (error) {
                            reject(error);
                        }
                        else {
                            resolve();
                        }
                    });
                });

                var p2 = new Promise(function(resolve, reject) {
                    connection.query('UPDATE friend_list SET friend_name = ? WHERE friend_id = ?', [name, user_id], function (error, results, fields) {
                        if (error) {
                            reject(error);
                        }
                        else {
                            resolve();
                        }
                    });
                });

                var p3 = new Promise(function(resolve, reject) {
                    connection.query('UPDATE chat SET user_name = ? WHERE user_id = ?', [name, user_id], function (error, results, fields) {
                        if (error) {
                            reject(error);
                        }
                        else {
                            resolve();
                        }
                    });
                });

                var p4 = new Promise(function(resolve, reject) {
                    connection.query('UPDATE chat SET friend_name = ? WHERE friend_id = ?', [name, user_id], function (error, results, fields) {
                        if (error) {
                            reject(error);
                        }
                        else {
                            resolve();
                        }
                    });
                });

                let promise = Promise.all([p1, p2, p3, p4]);
                promise.then(function(data) {
                    connection.commit(function(error) {
                        if (error) {
                            throw error;
                        }
                        else {
                            connection.release();

                            res.send(JSON.stringify({status: 'success'}));
                        }
                    });
                }).catch(function(error) {
                    connection.rollback(function() {});
                    connection.release();

                    log('critical', error);
                    res.send(JSON.stringify({status: 'error'}));
                });
            });
        });
    });
});

app.post('/claimRewarded', function(req, res) {
    checkUserToken(req.body.token, req.body.game_id).then(function(user_id) {
        rds.getConnection(function(err, connection) {
            connection.beginTransaction(function(err) {
                var p1 = new Promise(function(resolve, reject) {
                    if (req.body.speed > 20 || req.body.laser > 10) reject('hacker::' + user_id);
                    else {
                        connection.query('UPDATE inventory SET speed = speed + ?, laser = laser + ? WHERE user_id = ?', [req.body.speed, req.body.laser, user_id], function (error, results, fields) {
                            if (error) {
                                reject(error);
                            }
                            else {
                                resolve();
                            }
                        });
                    }
                });

                let promise = Promise.all([p1]);
                promise.then(function(data) {
                    connection.commit(function(error) {
                        if (error) {
                            throw error;
                        }
                        else {
                            connection.release();

                            res.send(JSON.stringify({status: 'success'}));
                        }
                    });
                }).catch(function(error) {
                    connection.rollback(function() {});
                    connection.release();

                    log('critical', error);
                    res.send(JSON.stringify({status: 'error'}));
                });
            });
        });
    });
});

app.post('/claimShare', function(req, res) {
    checkUserToken(req.body.token, req.body.game_id).then(function(user_id) {
        rds.getConnection(function(err, connection) {
            connection.beginTransaction(function(err) {
                var p1 = new Promise(function(resolve, reject) {
                    if (req.body.speed > 30 || req.body.laser > 20) reject('hacker::' + user_id);
                    else {
                        connection.query('UPDATE inventory SET speed = speed + ?, laser = laser + ? WHERE user_id = ?', [req.body.speed, req.body.laser, user_id], function (error, results, fields) {
                            if (error) {
                                reject(error);
                            }
                            else {
                                resolve();
                            }
                        });
                    }
                });

                let promise = Promise.all([p1]);
                promise.then(function(data) {
                    connection.commit(function(error) {
                        if (error) {
                            throw error;
                        }
                        else {
                            connection.release();

                            res.send(JSON.stringify({status: 'success'}));
                        }
                    });
                }).catch(function(error) {
                    connection.rollback(function() {});
                    connection.release();

                    log('critical', error);
                    res.send(JSON.stringify({status: 'error'}));
                });
            });
        });
    });
});

app.post('/gameSession', function(req, res) {
    checkUserToken(req.body.token, req.body.game_id).then(function(user_id) {
        rds.getConnection(function(err, connection) {
            connection.beginTransaction(function(err) {
                rds.query('SELECT laser, speed FROM inventory WHERE user_id = ?', [user_id], function (error, results, fields) {
                    if (error) {
                        log('critical', error);
                        reject('cannot find inventory');
                    }
                    else {
                        var inventory = results;

                        var p1 = new Promise(function(resolve, reject) {
                            // if (req.body.speed > inventory.speed || req.body.laser > inventory.laser) reject('hacker::' + user_id);
                            // else {
                                connection.query('UPDATE inventory SET speed = ?, laser = ? WHERE user_id = ?', [req.body.speed, req.body.laser, user_id], function (error, results, fields) {
                                    if (error) {
                                        reject(error);
                                    }
                                    else {
                                        resolve();
                                    }
                                });
                            // }
                        });

                        var p2 = new Promise(function(resolve, reject) {
                            if (req.body.speed > inventory.speed || req.body.laser > inventory.laser) reject('hacker::' + user_id);
                            else {
                                var post = {
                                    user_id: user_id,
                                    duration: req.body.duration,
                                    tank: req.body.tank,
                                    eagle: req.body.king
                                };

                                connection.query('INSERT INTO game_session SET ?', post, function (error, results, fields) {
                                    if (error) {
                                        reject(error);
                                    }
                                    else {
                                        resolve();
                                    }
                                });
                            }
                        });

                        var p3 = new Promise(function(resolve, reject) {
                            connection.query('UPDATE score SET eagle = eagle + ?, tank = tank + ?, points = points + ? WHERE user_id = ?', [req.body.king, req.body.tank, (req.body.king + req.body.tank), user_id], function (error, results, fields) {
                                if (error) {
                                    reject(error);
                                }
                                else {
                                    resolve();
                                }
                            });
                        });

                        let promise = Promise.all([p1, p2, p3]);
                        promise.then(function(data) {
                            connection.commit(function(error) {
                                if (error) {
                                    throw error;
                                }
                                else {
                                    connection.release();

                                    res.send(JSON.stringify({status: 'success'}));
                                }
                            });
                        }).catch(function(error) {
                            connection.rollback(function() {});
                            connection.release();

                            log('critical', error);
                            res.send(JSON.stringify({status: 'error'}));
                        });
                    }
                });
            });
        });
    });
});

app.post('/fbSubscribeBot', function(req, res) {
    checkUserToken(req.body.token, req.body.game_id).then(function(user_id) {
        rds.query('UPDATE user SET fbinstant_bot = 1 WHERE user_id = ?', [user_id], function (error, results, fields) {
            if (error) {
                log('critical', error);
                res.send(JSON.stringify({status: 'error'}));
            }
            else {
                res.send(JSON.stringify({status: 'success'}));
            }
        });
    });
});


/**
**************************************************
*
* friend
*
**************************************************
*/


app.post('/blockFriend', function(req, res) {
    checkUserToken(req.body.token, req.body.game_id).then(function(user_id) {
        rds.getConnection(function(err, connection) {
            connection.beginTransaction(function(err) {
                var p1 = new Promise(function(resolve, reject) {
                    connection.query('SELECT id FROM friend_list WHERE user_id = ? AND friend_id = ?', [user_id, req.body.id], function (error, results, fields) {
                        if (error) {
                            reject(error);
                        }
                        else {
                            resolve(results[0].id);
                        }
                    });
                });

                let promise = Promise.all([p1]);
                promise.then(function(data) {
                    var friend_id = data[0];

                    // **************************************************
                    // check friend
                    // **************************************************
                    if (!friend_id) {
                        throw new Error('invalid friend');
                    }


                    connection.query('UPDATE friend_list SET status = \'block\' WHERE id = ?', [friend_id], function (error, results, fields) {
                        if (error) {
                            throw error;
                        }
                        else {
                            connection.commit(function(error) {
                                if (error) {
                                    throw error;
                                }
                                else {
                                    connection.release();

                                    res.send(JSON.stringify({status: 'success'}));
                                }
                            });
                        }
                    });
                }).catch(function(error) {
                    connection.rollback(function() {});
                    connection.release();

                    log('critical', error);
                    res.send(JSON.stringify({status: 'error'}));
                });
            });
        });
    });
});

app.post('/deleteFriend', function(req, res) {
    checkUserToken(req.body.token, req.body.game_id).then(function(user_id) {
        rds.getConnection(function(err, connection) {
            connection.beginTransaction(function(err) {
                var p1 = new Promise(function(resolve, reject) {
                    connection.query('SELECT id FROM friend_list WHERE user_id = ? AND friend_id = ?', [user_id, req.body.id], function (error, results, fields) {
                        if (error) {
                            reject(error);
                        }
                        else {
                            resolve(results[0].id);
                        }
                    });
                });

                let promise = Promise.all([p1]);
                promise.then(function(data) {
                    var row_id = data[0];

                    // **************************************************
                    // check friend
                    // **************************************************
                    if (!row_id) {
                        throw new Error('invalid friend');
                    }


                    connection.query('UPDATE friend_list SET status = \'delete\' WHERE id = ?', [row_id], function (error, results, fields) {
                        if (error) {
                            throw error;
                        }
                        else {
                            connection.commit(function(error) {
                                if (error) {
                                    throw error;
                                }
                                else {
                                    connection.release();

                                    res.send(JSON.stringify({status: 'success'}));
                                }
                            });
                        }
                    });
                }).catch(function(error) {
                    connection.rollback(function() {});
                    connection.release();

                    log('critical', error);
                    res.send(JSON.stringify({status: 'error'}));
                });
            });
        });
    });
});

app.post('/acceptFriend', function(req, res) {
    checkUserToken(req.body.token, req.body.game_id).then(function(user_id) {
        rds.getConnection(function(err, connection) {
            connection.beginTransaction(function(err) {
                var p1 = new Promise(function(resolve, reject) {
                    connection.query('SELECT id FROM friend_list WHERE user_id = ? AND friend_id = ?', [user_id, req.body.id], function (error, results, fields) {
                        if (error) {
                            reject(error);
                        }
                        else {
                            resolve(results[0].id);
                        }
                    });
                });

                let promise = Promise.all([p1]);
                promise.then(function(data) {
                    var row_id = data[0];

                    // **************************************************
                    // check friend
                    // **************************************************
                    if (!row_id) {
                        throw new Error('invalid friend');
                    }


                    connection.query('UPDATE friend_list SET status = \'accept\' WHERE id = ?', [row_id], function (error, results, fields) {
                        if (error) {
                            throw error;
                        }
                        else {
                            connection.commit(function(error) {
                                if (error) {
                                    throw error;
                                }
                                else {
                                    connection.release();

                                        rds.query('SELECT * FROM friend_list WHERE user_id = ? AND (status=\'accept\' OR status = \'request\')', [user_id], function (error, results, fields) {
                                        if (error) {
                                            throw error;
                                        }
                                        else {
                                            for (var i = 0; i < results.length; i++) {
                                                delete results[i].id;
                                                delete results[i].user_id;
                                                delete results[i].datetime_created;
                                                delete results[i].datetime_updated;
                                            }

                                            res.send(JSON.stringify({status: 'success', friends: results}));
                                        }
                                    });
                                }
                            });
                        }
                    });
                }).catch(function(error) {
                    connection.rollback(function() {});
                    connection.release();

                    log('critical', error);
                    res.send(JSON.stringify({status: 'error'}));
                });
            });
        });
    });
});

app.post('/searchFriend', function(req, res) {
    checkUserToken(req.body.token, req.body.game_id).then(function(user_id) {
        var p1 = new Promise(function(resolve, reject) {
            rds.query('SELECT name, user_id, photo FROM user WHERE game_id = ?', [req.body.id], function (error, results, fields) {
                if (error) {
                    reject(error);
                }
                else {
                    resolve(results[0]);
                }
            });
        });

        let promise = Promise.all([p1]);
        promise.then(function(data) {
            var friend = data[0];


            // **************************************************
            // check friend
            // **************************************************
            if (!friend) res.send(JSON.stringify({status: 'error'}));
            else res.send(JSON.stringify({status: 'success', friend: friend}));
        }).catch(function(error) {
            log('critical', error);
            res.send(JSON.stringify({status: 'error'}));
        });

    });
});

app.post('/addFriend', function(req, res) {
    checkUserToken(req.body.token, req.body.game_id).then(function(user_id) {
        rds.getConnection(function(err, connection) {
            connection.beginTransaction(function(err) {
                var p1 = new Promise(function(resolve, reject) {
                    connection.query('SELECT * FROM user WHERE user_id = ?', [req.body.id], function (error, results, fields) {
                        if (error) {
                            reject(error);
                        }
                        else {
                            resolve(results[0]);
                        }
                    });
                });

                var p2 = new Promise(function(resolve, reject) {
                    connection.query('SELECT * FROM user WHERE user_id = ?', [user_id], function (error, results, fields) {
                        if (error) {
                            reject(error);
                        }
                        else {
                            resolve(results[0]);
                        }
                    });
                });

                var p3 = new Promise(function(resolve, reject) {
                    connection.query('SELECT * FROM friend_list WHERE user_id = ? AND friend_id = ?', [user_id, req.body.id], function (error, results, fields) {
                        if (error) {
                            reject(error);
                        }
                        else {
                            resolve(results[0]);
                        }
                    });
                });

                let promise = Promise.all([p1, p2, p3]);
                promise.then(function(data) {
                    var friend = data[0];
                    var user = data[1];
                    var exist = data[2];

                    // **************************************************
                    // check friend
                    // **************************************************
                    if (!friend) {
                        throw new Error('invalid friend');
                    }

                    // **************************************************
                    // check friend exist
                    // **************************************************
                    if (exist) {
                        connection.rollback(function() {});
                        connection.release();
                        
                        rds.query('SELECT * FROM friend_list WHERE user_id = ? AND (status=\'accept\' OR status = \'request\')', [user_id], function (error, results, fields) {
                            if (error) {
                                throw error;
                            }
                            else {
                                for (var i = 0; i < results.length; i++) {
                                    delete results[i].id;
                                    delete results[i].user_id;
                                    delete results[i].datetime_created;
                                    delete results[i].datetime_updated;
                                }

                                res.send(JSON.stringify({status: 'success', friends: results}));
                            }
                        });
                    }
                    else {
                        var p4 = new Promise(function(resolve, reject) {
                            var post = {};
                            post.user_id = user_id;
                            post.friend_id = friend.user_id;
                            post.friend_name = friend.name;
                            post.friend_photo = friend.photo;
                            post.status = 'accept';

                            connection.query('INSERT INTO friend_list SET ?', post, function (error, results, fields) {
                                if (error) {
                                    reject(error);
                                }
                                else {
                                    resolve();
                                }
                            });
                        });

                        var p5 = new Promise(function(resolve, reject) {
                            var post = {};
                            post.user_id = friend.user_id;
                            post.friend_id = user_id;
                            post.friend_name = user.name;
                            post.friend_photo = user.photo;
                            post.status = 'request';

                            connection.query('INSERT INTO friend_list SET ?', post, function (error, results, fields) {
                                if (error) {
                                    reject(error);
                                }
                                else {
                                    resolve();
                                }
                            });
                        });

                        var p6 = new Promise(function(resolve, reject) {
                            connection.query('UPDATE user_data SET new_friend_request = new_friend_request + 1 WHERE user_id = ?', [friend.user_id], function (error, results, fields) {
                                if (error) {
                                    log('critical', error);
                                    reject('cannot update user data');
                                }
                                else {
                                    resolve();
                                }
                            });
                        });

                        let promise = Promise.all([p4, p5, p6]);
                        promise.then(function(data) {
                            connection.commit(function(error) {
                                if (error) {
                                    throw error;
                                }
                                else {
                                    connection.release();

                                    rds.query('SELECT * FROM friend_list WHERE user_id = ? AND (status=\'accept\' OR status = \'request\')', [user_id], function (error, results, fields) {
                                        if (error) {
                                            throw error;
                                        }
                                        else {
                                            for (var i = 0; i < results.length; i++) {
                                                delete results[i].id;
                                                delete results[i].user_id;
                                                delete results[i].datetime_created;
                                                delete results[i].datetime_updated;
                                            }

                                            res.send(JSON.stringify({status: 'success', friends: results}));
                                        }
                                    });
                                }
                            });
                        }).catch(function(error) {
                            connection.rollback(function() {});
                            connection.release();

                            log('critical', error);
                            res.send(JSON.stringify({status: 'error'}));
                        });
                    }
                }).catch(function(error) {
                    connection.rollback(function() {});
                    connection.release();

                    log('critical', error);
                    res.send(JSON.stringify({status: 'error'}));
                });
            });
        });
    });
});

app.post('/friendRooms', function(req, res) {
    checkUserToken(req.body.token, req.body.game_id).then(function(user_id) {
        try {
            var friends = JSON.parse(decodeURIComponent(req.body.friends));

            var promises = [];

            for (var i = 0; i < friends.length; i++) {
                var p = new Promise(function(resolve, reject) {
                    var friend_id = friends[i].friend_id;
                    redis.hget('player_room', friend_id).then(function(room_id) {
                        resolve([friend_id, room_id]);
                    });
                });

                promises.push(p);
            }

            let promise = Promise.all(promises);
            promise.then(function(data) {
                res.send(JSON.stringify({status: 'success', data: data}));
            });
        } catch (err) {
            log('critical', err);
            res.send(JSON.stringify({status: 'error'}));
        }
    });
});


/**
**************************************************
*
* chat
*
**************************************************
*/


app.post('/sendChat', function(req, res) {
    checkUserToken(req.body.token, req.body.game_id).then(function(user_id) {
        rds.getConnection(function(err, connection) {
            connection.beginTransaction(function(err) {
                var p1 = new Promise(function(resolve, reject) {
                    connection.query('SELECT * FROM chat WHERE (user_id = ? AND friend_id = ?) OR (user_id = ? AND friend_id = ?)', [user_id, req.body.id, req.body.id, user_id], function (error, results, fields) {
                        if (error) {
                            reject(error);
                        }
                        else {
                            if (results.length > 0) {
                                resolve(results[0]);
                            }
                            else {
                                connection.query('SELECT user_id, name FROM user WHERE user_id = ? OR user_id = ?', [user_id, req.body.id], function (error, results, fields) {
                                    if (error) {
                                        reject(error);
                                    }
                                    else {
                                        if (results[0].user_id == user_id) {
                                            var user_name = results[0].name;
                                            var friend_name = results[1].name;
                                        }
                                        else {
                                            var user_name = results[1].name;
                                            var friend_name = results[0].name;
                                        }

                                        var post = {};
                                        post.user_id = user_id;
                                        post.user_name = user_name
                                        post.friend_id = req.body.id;
                                        post.friend_name = friend_name;

                                        connection.query('INSERT INTO chat SET ?', post, function (error, results, fields) {
                                            if (error) {
                                                reject(error);
                                            }
                                            else {
                                                connection.query('SELECT * FROM chat WHERE id = ?', [results.insertId], function (error, results, fields) {
                                                    if (error) {
                                                        reject(error);
                                                    }
                                                    else {
                                                        resolve(results[0]);
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    });
                });

                let promise = Promise.all([p1]);
                promise.then(function(data) {
                    var chat = data[0];

                    var p2 = new Promise(function(resolve, reject) {
                        connection.query('UPDATE user_data SET new_chat_message = new_chat_message + 1 WHERE user_id = ?', [req.body.id], function (error, results, fields) {
                            if (error) {
                                reject(error);
                            }
                            else {
                                resolve();
                            }
                        });
                    });

                    var p3 = new Promise(function(resolve, reject) {
                        var post = {};
                        post.chat_id = chat.id;
                        post.sender = user_id;
                        post.message = req.body.message;

                        connection.query('INSERT INTO chat_message SET ?', post, function (error, results, fields) {
                            if (error) {
                                reject(error);
                            }
                            else {
                                resolve(results.insertId);
                            }
                        });
                    });


                    let promise = Promise.all([p2, p3]);
                    promise.then(function(data) {
                        connection.commit(function(error) {
                            if (error) {
                                throw error;
                            }
                            else {
                                connection.release();

                                rds.query('SELECT chat_id, sender, message, datetime_created FROM chat_message WHERE id = ?', [data[1]], function (error, results, fields) {
                                    if (error) {
                                        throw error;
                                    }
                                    else {
                                        res.send(JSON.stringify({status: 'success', chat: chat, message: results[0]}));
                                    }
                                });
                            }
                        });
                    }).catch(function(error) {
                        connection.rollback(function() {});
                        connection.release();

                        log('critical', error);
                        res.send(JSON.stringify({status: 'error'}));
                    });
                }).catch(function(error) {
                    connection.rollback(function() {});
                    connection.release();

                    log('critical', error);
                    res.send(JSON.stringify({status: 'error'}));
                });
            });
        });
    });
});

app.post('/deleteChat', function(req, res) {
    checkUserToken(req.body.token, req.body.game_id).then(function(user_id) {
        rds.getConnection(function(err, connection) {
            connection.beginTransaction(function(err) {
                var p1 = new Promise(function(resolve, reject) {
                    connection.query('SELECT * FROM chat WHERE (user_id = ? OR friend_id = ?) AND id = ?', [user_id, user_id, req.body.id], function (error, results, fields) {
                        if (error) {
                            reject(error);
                        }
                        else {
                            if (results.length > 0) {
                                resolve(results[0]);
                            }
                            else {
                                reject('no chat found');
                            }
                        }
                    });
                });

                let promise = Promise.all([p1]);
                promise.then(function(data) {
                    var chat = data[0];

                    var p2 = new Promise(function(resolve, reject) {
                        connection.query('UPDATE chat_message SET sender_status = \'delete\' WHERE sender = ? AND chat_id = ?', [user_id, chat.id], function (error, results, fields) {
                            if (error) {
                                reject(error);
                            }
                            else {
                                resolve();
                            }
                        });
                    });

                    var p3 = new Promise(function(resolve, reject) {
                        connection.query('UPDATE chat_message SET recipent_status = \'delete\' WHERE sender != ? AND chat_id = ?', [user_id, chat.id], function (error, results, fields) {
                            if (error) {
                                reject(error);
                            }
                            else {
                                resolve();
                            }
                        });
                    });

                    let promise = Promise.all([p2, p3]);
                    promise.then(function(data) {
                        connection.commit(function(error) {
                            if (error) {
                                throw error;
                            }
                            else {
                                connection.release();

                                res.send(JSON.stringify({status: 'success'}));
                            }
                        });
                    }).catch(function(error) {
                        connection.rollback(function() {});
                        connection.release();

                        log('critical', error);
                        res.send(JSON.stringify({status: 'error'}));
                    });
                }).catch(function(error) {
                    connection.rollback(function() {});
                    connection.release();

                    log('critical', error);
                    res.send(JSON.stringify({status: 'error'}));
                });
            });
        });
    });
});

app.post('/blockChat', function(req, res) {
    checkUserToken(req.body.token, req.body.game_id).then(function(user_id) {
        rds.getConnection(function(err, connection) {
            connection.beginTransaction(function(err) {
                var p1 = new Promise(function(resolve, reject) {
                    connection.query('SELECT * FROM chat WHERE (user_id = ? OR friend_id = ?) AND id = ?', [user_id, user_id, req.body.id], function (error, results, fields) {
                        if (error) {
                            reject(error);
                        }
                        else {
                            if (results.length > 0) {
                                resolve(results[0]);
                            }
                            else {
                                reject('no chat found');
                            }
                        }
                    });
                });

                let promise = Promise.all([p1]);
                promise.then(function(data) {
                    var chat = data[0];

                    var p2 = new Promise(function(resolve, reject) {
                        if (chat.user_id == user_id) {
                            connection.query('UPDATE chat SET user_status = \'block\' WHERE id = ?', [chat.id], function (error, results, fields) {
                                if (error) {
                                    reject(error);
                                }
                                else {
                                    resolve();
                                }
                            });
                        }
                        else {
                            connection.query('UPDATE chat SET friend_status = \'block\' WHERE id = ?', [chat.id], function (error, results, fields) {
                                if (error) {
                                    reject(error);
                                }
                                else {
                                    resolve();
                                }
                            });
                        }
                    });

                    let promise = Promise.all([p2]);
                    promise.then(function(data) {
                        connection.commit(function(error) {
                            if (error) {
                                throw error;
                            }
                            else {
                                connection.release();

                                res.send(JSON.stringify({status: 'success'}));
                            }
                        });
                    }).catch(function(error) {
                        connection.rollback(function() {});
                        connection.release();

                        log('critical', error);
                        res.send(JSON.stringify({status: 'error'}));
                    });
                }).catch(function(error) {
                    connection.rollback(function() {});
                    connection.release();

                    log('critical', error);
                    res.send(JSON.stringify({status: 'error'}));
                });
            });
        });
    });
});

app.post('/reportChat', function(req, res) {
    checkUserToken(req.body.token, req.body.game_id).then(function(user_id) {
        rds.getConnection(function(err, connection) {
            connection.beginTransaction(function(err) {
                var p1 = new Promise(function(resolve, reject) {
                    connection.query('SELECT * FROM chat WHERE (user_id = ? OR friend_id = ?) AND id = ?', [user_id, user_id, req.body.id], function (error, results, fields) {
                        if (error) {
                            reject(error);
                        }
                        else {
                            if (results.length > 0) {
                                resolve(results[0]);
                            }
                            else {
                                reject('no chat found');
                            }
                        }
                    });
                });

                let promise = Promise.all([p1]);
                promise.then(function(data) {
                    var chat = data[0];

                    var p2 = new Promise(function(resolve, reject) {
                        if (chat.user_id == user_id) {
                            connection.query('UPDATE chat SET user_status = \'block\' WHERE id = ?', [chat.id], function (error, results, fields) {
                                if (error) {
                                    reject(error);
                                }
                                else {
                                    resolve();
                                }
                            });
                        }
                        else {
                            connection.query('UPDATE chat SET friend_status = \'block\' WHERE id = ?', [chat.id], function (error, results, fields) {
                                if (error) {
                                    reject(error);
                                }
                                else {
                                    resolve();
                                }
                            });
                        }
                    });

                    var p3 = new Promise(function(resolve, reject) {
                        var post = {};
                        post.chat_id = chat.id;
                        post.reported_by = user_id;

                        if (chat.user_id == user_id) {
                            post.reported_against = chat.friend_id;
                        }
                        else {
                            post.reported_against = user_id;
                        }

                        connection.query('INSERT INTO chat_report SET ?', post, function (error, results, fields) {
                            if (error) {
                                reject(error);
                            }
                            else {
                                resolve();
                            }
                        });
                    });

                    let promise = Promise.all([p2, p3]);
                    promise.then(function(data) {
                        connection.commit(function(error) {
                            if (error) {
                                throw error;
                            }
                            else {
                                connection.release();

                                res.send(JSON.stringify({status: 'success'}));
                            }
                        });
                    }).catch(function(error) {
                        connection.rollback(function() {});
                        connection.release();

                        log('critical', error);
                        res.send(JSON.stringify({status: 'error'}));
                    });
                }).catch(function(error) {
                    connection.rollback(function() {});
                    connection.release();

                    log('critical', error);
                    res.send(JSON.stringify({status: 'error'}));
                });
            });
        });
    });
});



/**
**************************************************
*
* IAP
*
**************************************************
*/
app.post('/makePurchase', function(req, res) {
    checkUserToken(req.body.token, req.body.game_id).then(function(user_id) {
        rds.getConnection(function(err, connection) {
            connection.beginTransaction(function(err) {
                var p1 = new Promise(function(resolve, reject) {
                    var post = {
                        user_id: user_id,
                        type: 'Purchase',
                        transaction_id: req.body.transaction_id,
                        iap: req.body.iap,
                        source: req.body.source
                    };

                    connection.query('INSERT INTO user_purchase SET ?', post, function (error, results, fields) {
                        if (error) {
                            reject(error);
                        }
                        else {
                            resolve();
                        }
                    });
                });

                var p2 = new Promise(function(resolve, reject) {
                    var iap = req.body.iap;
                    switch (iap) {
                        case '1':
                            connection.query('SELECT `unlock` FROM user_data WHERE user_id = ?', [user_id], function (error, results, fields) {
                                if (error) {
                                    reject(error);
                                }
                                else {
                                    var unlock = JSON.parse(results[0].unlock);
                                    if (!unlock.includes('skin')) unlock.push('skin');
                                    if (!unlock.includes('name')) unlock.push('name');

                                    connection.query('UPDATE user_data SET `unlock` = ? WHERE user_id = ?', [JSON.stringify(unlock), user_id], function (error, results, fields) {
                                        if (error) {
                                            reject(error);
                                        }
                                        else {
                                            resolve();
                                        }
                                    });
                                }
                            });

                            break;
                        case '2':
                            connection.query('SELECT `unlock` FROM user_data WHERE user_id = ?', [user_id], function (error, results, fields) {
                                if (error) {
                                    reject(error);
                                }
                                else {
                                    var unlock = JSON.parse(results[0].unlock);
                                    if (!unlock.includes('ads')) unlock.push('ads');

                                    connection.query('UPDATE user_data SET `unlock` = ? WHERE user_id = ?', [JSON.stringify(unlock), user_id], function (error, results, fields) {
                                        if (error) {
                                            reject(error);
                                        }
                                        else {
                                            connection.query('UPDATE inventory SET laser = laser + ?, speed = speed + ? WHERE user_id = ?', [999, 999, user_id], function (error, results, fields) {
                                                if (error) {
                                                    reject(error);
                                                }
                                                else {
                                                    resolve();
                                                }
                                            });
                                        }
                                    });
                                }
                            });

                            break;
                        case '3':
                            connection.query('UPDATE inventory SET laser = laser + ?, speed = speed + ? WHERE user_id = ?', [999, 999, user_id], function (error, results, fields) {
                                if (error) {
                                    reject(error);
                                }
                                else {
                                    resolve();
                                }
                            });

                            break;
                        default:
                            resolve();
                            break;
                    }
                    
                });

                let promise = Promise.all([p1, p2]);
                promise.then(function(data) {
                    connection.commit(function(error) {
                        if (error) {
                            throw error;
                        }
                        else {
                            connection.release();

                            res.send(JSON.stringify({status: 'success'}));
                        }
                    });
                }).catch(function(error) {
                    connection.rollback(function() {});
                    connection.release();

                    log('critical', error);
                    res.send(JSON.stringify({status: 'error'}));
                });
            });
        });
    });
});

app.post('/consumePurchase', function(req, res) {
    checkUserToken(req.body.token, req.body.game_id).then(function(user_id) {
        var post = {
            user_id: user_id,
            type: 'Consume',
            transaction_id: req.body.transaction_id
        };

        rds.query('INSERT INTO user_purchase SET ?', post, function (error, results, fields) {
            if (error) {
                log('critical', error);
                res.send(JSON.stringify({status: 'error'}));
            }
            else {
                res.send(JSON.stringify({status: 'success'}));
            }
        });
    });
});


app.post('/debug', function(req, res) {
    console.log(req.body);
    res.send(JSON.stringify({status: 'success'}));
});



/**
**************************************************
*
* socket
*
**************************************************
*/


// if reconnected, it's a new socket
btr.on('connection', socket => {
    /**************************************************
     * socket events
     **************************************************/    
    socket.on('disconnect', (reason) => {
        if (socket.room_id) {
            redis.zincrby('room_count', -1, socket.room_id);

            // update room force
            if (socket.force == 'Red') redis.hincrby('room_force:' + socket.room_id, 'Red', -1);
            else redis.hincrby('room_force:' + socket.room_id, 'Blue', -1);

            if (socket.player_id) {
                // remove player from room
                redis.hget('room_players', socket.room_id).then(function(players) {
                    players = JSON.parse(players);

                    delete players[socket.player_id];

                    redis.hset('room_players', socket.room_id, JSON.stringify(players));
                });


                // delete player room
                redis.hdel('player_room', socket.player_id);


                btr.in(socket.room_id).binary(false).emit('player-left', socket.player_id);
            }


            // cleanup check
            btr.in(socket.room_id).clients((error, clients) => {
                if (error) log(error);
                
                redis.zscore('room_count', socket.room_id).then(function(score) {
                    // no player can join and eventually the room will die
                    if (score != clients.length) {
                        log('close room ' + socket.room_id, socket.room_id, score);
                        redis.zincrby('room_count', 100, socket.room_id);
                    }
                });
            });
        }
    });

    socket.on('error', (error) => {
        socket.disconnect(true);
    });

    socket.on('join-room', function (data) {
        new Promise(function(main_resolve, main_reject) {
            /**************************************************
             * prevent player from multiple devices
             **************************************************/
             redis.hget('room_players', data.player_id).then(function(room_id) {
                if (room_id) {
                    socket.binary(false).emit('join-room', {
                        success: false
                    });

                    main_reject(data.player_id + ' playing with multiple devices');
                }
                else {
                    // console.log(data.room_id);
                    if (!data.room_id) {

                        /**************************************************
                         * by doing this, if a room count < 0, no player can join and eventually the room will die
                         *
                         * if a room count > Const.room_max - 1, no player can join and eventually the room will die
                         *
                         * limit real players to 10
                         **************************************************/
                        redis.zrangebyscore('room_count', 0, 9, 'withscores', function(err, rooms) {
                            new Promise(function(resolve, reject) {
                                if (rooms.length <= 0) {
                                    /**************************************************
                                     * create new room
                                     **************************************************/
                                    var room_id = chance.guid();
                                    log('info', 'Room ' + room_id + ' created.');

                                    redis.hset('rooms', room_id, JSON.stringify({
                                        created: Date.now()
                                    })).then(function(res) {
                                        data.room_id = room_id;
                                        resolve();
                                    });
                                } else {
                                    data.room_id = rooms[0];
                                    resolve();
                                }
                            }).then(function() {
                                main_resolve(false);
                            });
                        });
                    }
                    else {
                        main_resolve(true);
                    }
                }
             });
        }).then(function(rejoin) {
            btr.adapter.remoteJoin(socket.id, data.room_id, (err) => {
                if (err) {
                    log('critical', error);
                } else {
                    redis.zincrby('room_count', 1, data.room_id);

                    // get current room players info
                    redis.hget('room_players', data.room_id).then(function(players) {
                        if (!players) players = {};
                        else players = JSON.parse(players);

                        redis.hgetall('room_force:' + data.room_id).then(function(room_force) {
                            var blue = 0, red = 0, force = data.force;

                            var keys = Object.keys(room_force);
                            if (keys.length == 0) {
                                redis.hincrby('room_force:' + data.room_id, 'Red', 0);
                                redis.hincrby('room_force:' + data.room_id, 'Blue', 0);
                            }

                            for (var i = 0; i < keys.length; i++) {
                                if (keys[i] == 'Blue') blue = parseInt(room_force[keys[i]]);
                                else red = parseInt(room_force[keys[i]]);
                            }


                            var info = {
                                player_id: data.player_id,
                                player_name: data.player_name,
                                player_photo: data.player_photo,
                                player_force: force,
                                player_position: data.player_position,
                                player_tank: data.player_tank
                            };


                            if (rejoin) {
                            }
                            else {
                                // determine player force
                                if (blue > red) {
                                    force = 'Red';
                                    red++;
                                }
                                else {
                                    force = 'Blue';
                                    blue++;
                                }

                                info.player_force = force;
                            }

                            if (force == 'Blue') redis.hincrby('room_force:' + data.room_id, 'Blue', 1);
                            else redis.hincrby('room_force:' + data.room_id, 'Red', 1);


                            // broadcast player info
                            socket.binary(false).to(data.room_id).emit('player-join', info);


                            // save player info
                            delete info['player_position'];
                            players[data.player_id] = info
                            redis.hset('room_players', data.room_id, JSON.stringify(players));


                            // save player room
                            redis.hset('player_room', data.player_id, data.room_id);


                            // need to set everytime because server can go down
                            socket.player_id = data.player_id;
                            socket.room_id = data.room_id;
                            socket.force = force;


                            // send info to player
                            var room = {
                                room_id: data.room_id,
                                room_max: Const.room_max,
                                blue: blue,
                                red: red
                            }
                            socket.binary(false).emit('join-room', {
                                success: true,
                                force: socket.force,
                                players: players,
                                room: room
                            });
                        });
                    });
                }
            });
        })
        .catch(function(error) {
            log('critical', error);
        });
    });

    socket.on('update', function (data) {
        redis.sadd('room_update:' + socket.room_id, JSON.stringify(data));
    });

    socket.on('king_hit', function (data) {
        redis.hincrby('room_king:' + socket.room_id, 'health', -1);
    });

    socket.on('king_wood_hit', function (data) {
        redis.hset('room_king:' + socket.room_id, 'wood_' + data.position, true);
    });
});























