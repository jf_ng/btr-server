'use strict';

var Const = require('./server-const.js');

var io = require('socket.io-emitter')({ host: '127.0.0.1', port: 6379, key: 'btr' });

const btr = io.of('/btr');

var Redis = require("ioredis");
var redis = new Redis(6379, '127.0.0.1');

var schedule_freq = 60000; // @todo

var king_tick = 0;
var king_tick_announce = 2;

var update_tick = 0;


/**
**************************************************
*
* king
*
**************************************************
*/
var king, king_startTime, timeout = 0;

/*
announce appear
appear
announce disappear
disappear
dead
*/
var king_status;

new Promise(function(resolve, reject) {
    /**************************************************
    * clear all updates
    **************************************************/
    redis.hgetall('rooms').then(function(rooms) {
        var keys = Object.keys(rooms);
        for (var i = 0; i < keys.length; i++) {
            redis.del('room_update:' + keys[i]);
        }
    });


    /**************************************************
    * determine king and time
    **************************************************/
    redis.get('king').then(function(data) {
        if (data) {
            data = JSON.parse(data);

            king = data.king;
            king_startTime = data.king_startTime;

            // check if king expired
            var now = Date.now();

            if (Object.keys(data) == 0) {

            }
            else if (now - king_startTime >= Const.king_duration) {
                king = null; king_startTime = null;
                redis.set('king', JSON.stringify({
                    king: king,
                    king_startTime: king_startTime
                }));
            }
            else {
                king_status = 'appear';

                timeout = (king_startTime + Const.king_duration) - now;
            }
        }
        else {
            redis.set('king', JSON.stringify({
                king: king,
                king_startTime: king_startTime
            }));
        }

        resolve();
    });
}).then(function() {
    setTimeout(function() {
        setInterval(function() {
            king_tick++;
            console.log(king_tick)
            // console.log(king_tick, Const.king_interval / schedule_freq, king_tick_announce, king_tick % ((Const.king_interval / schedule_freq) - king_tick_announce));

            if (king_tick % (Const.king_interval / schedule_freq) == 0) {
                if (king == 'Blue') king = 'Red';
                else king = 'Blue';

                var now = Date.now();

                redis.set('king', JSON.stringify({
                    king: king,
                    king_startTime: now
                }));

                king_status = 'appear';
                king_startTime = now;


                // set all room king health to 10, and del wood
                redis.hgetall('rooms').then(function(rooms) {
                    var keys = Object.keys(rooms);
                    for (var i = 0; i < keys.length; i++) {
                        redis.hset('room_king:' + keys[i], 'health', 10);
                        redis.hdel('room_king:' + keys[i], 'wood_left', 'wood_right', 'wood_front');
                    }
                });


                btr.emit('king', {
                    'force': king,
                    'duration': Const.king_duration,
                    'status': king_status
                });

                console.log('appear', king);
            }
            else if (king_tick % (Const.king_interval / schedule_freq) == king_tick_announce) {
                king_status = 'announce appear';

                btr.emit('king', {
                    'force': (king == 'Blue') ? 'Red' : 'Blue',
                    'duration': schedule_freq,
                    'status': king_status
                });

                console.log('announce appear', ((king == 'Blue') ? 'Red' : 'Blue'));
            }
        }, schedule_freq);
    }, timeout);
});


/**************************************************
*
* update
*
**************************************************
*/
var roomUpdates = function(room_id) {
    redis.spop('room_update:' + room_id, 50).then(function(updates) {
        new Promise(function(resolve, reject) {
            if (king_status == 'appear') {
                if (updates.length > 0) {

                    /*
                    * king health is <10 will be -1 less than actual
                    * because this is processed and emit first before health is deducted
                    */
                    redis.hgetall('room_king:' + room_id).then(function(room_king) {
                        updates.push({
                            'player_id': 'king', // ride on this update instead of emit king (then 2 emits)
                            'room_king': room_king,

                            'status': king_status,
                            'force': king,
                            'duration': Const.king_duration - (Date.now() - king_startTime)
                        });

                        resolve();
                    });
                }
                else {
                    resolve();
                }
            }
            else if (king_status == 'announce disappear') {
                updates.push({
                    'player_id': 'king', // ride on this update instead of emit king (then 2 emits)

                    'status': king_status,
                    'force': king,
                    'duration': 0
                });

                console.log('announce disappear', king);

                resolve();
            }
            else {
                resolve();
            }
        }).then(function() {
            if (updates.length > 0) btr.to(room_id).emit('updates', updates);
        });
    });
};

setInterval(function() {
    // announce disappear, disappear
    if (king_status == 'appear' || king_status == 'announce disappear') {
        update_tick++;

        if (update_tick % 5 == 0) {
            if (Date.now() - (king_startTime + Const.king_duration) >= 0) {
                king_status = 'announce disappear';
            }
        }
        else if (king_status == 'announce disappear') {
            king_status = 'disappear';
            update_tick = 0;
        }
    }

    redis.hgetall('rooms').then(function(rooms) {
        var keys = Object.keys(rooms);
        for (var i = 0; i < keys.length; i++) {
            roomUpdates(keys[i]);
        }
    });
}, 200);